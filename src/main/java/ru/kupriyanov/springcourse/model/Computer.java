package ru.kupriyanov.springcourse.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kupriyanov.springcourse.enumerated.MusicTypes;

import java.util.UUID;

@Component
public class Computer {

    private final String id;

    private final MusicPlayer musicPlayer;

    @Autowired
    public Computer(MusicPlayer musicPlayer) {
        this.id = UUID.randomUUID().toString();
        this.musicPlayer = musicPlayer;
    }

    @Override
    public String toString() {
        return "Computer " + id + " is playing " + musicPlayer.playMusic(MusicTypes.CLASSICAL);
    }

}
