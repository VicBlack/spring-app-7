package ru.kupriyanov.springcourse.model;

import org.springframework.stereotype.Component;
import ru.kupriyanov.springcourse.api.IMusic;

// создание бина через аннотацию
@Component("classicMusicBean")
public class ClassicalMusic implements IMusic {

    private final String[] classicalSongsList = {"Classical_1", "Classical_2", "Classical_3"};

    @Override
    public String[] getMusic() {
        return classicalSongsList;
    }

}
