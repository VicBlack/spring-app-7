package ru.kupriyanov.springcourse.model;

import org.springframework.stereotype.Component;
import ru.kupriyanov.springcourse.api.IMusic;

@Component("rockMusicBean")
public class RockMusic implements IMusic {

    private final String[] rockSongsList = {"Rock_1", "Rock_2", "Rock_3"};

    @Override
    public String[] getMusic() {
        return rockSongsList;
    }

}
