package ru.kupriyanov.springcourse.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.kupriyanov.springcourse.enumerated.MusicTypes;
import ru.kupriyanov.springcourse.api.IMusic;

import java.util.Random;

@Component
public class MusicPlayer {

//    @Autowired
    private final IMusic music;

    private final IMusic music2;

//    @Autowired
//    public MusicPlayer(IMusic music) {
//        this.music = music;
//    }

    @Autowired
    public MusicPlayer(@Qualifier("classicMusicBean") IMusic music,
                       @Qualifier("rockMusicBean") IMusic music2) {
        this.music = music;
        this.music2 = music2;
    }

//    @Autowired
//    public void setMusic(IMusic music) {
//        this.music = music;
//    }

    public String playMusic(MusicTypes musicType) {
        Random random = new Random();
        if (musicType == MusicTypes.CLASSICAL)
            return music.getMusic()[random.nextInt(music.getMusic().length)];
            else return music2.getMusic()[random.nextInt(music2.getMusic().length)];
//        System.out.println("Playing: " + classicalMusic.getMusic() + " and " + rockMusic.getMusic());
    }

}
