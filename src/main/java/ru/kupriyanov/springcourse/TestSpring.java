package ru.kupriyanov.springcourse;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.kupriyanov.springcourse.model.Computer;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

//        IMusic music = context.getBean("classicMusicBean", ClassicalMusic.class);
//        IMusic music2 = context.getBean("rockMusicBean", IMusic.class);

//        MusicPlayer musicPlayer = new MusicPlayer(classicalMusic);
//
//        musicPlayer.playMusic();

//        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
//        musicPlayer.playMusic();

        Computer computer = context.getBean("computer", Computer.class);
        System.out.println(computer);

        context.close();
    }

}